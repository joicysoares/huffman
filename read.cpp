#include "read.h"

read::read()
{

}

int* read::count(QString name){
    int* array=new int[256];
    for(int i=0;i<256;i++){
        array[i]=0;
    }
    QFile file(name);
    if(!file.open(QIODevice::ReadOnly)){
        qDebug()<< "Erro ao abir arquivo";
        exit (1);
    }

    while(!file.atEnd()){
        QByteArray line =file.readLine();
        for(int i=0;i<line.size();i++){
            ++array[(unsigned char)line.at(i)];
        }
    }
    file.close();
    return array;
}

void read::show(int *array){
    for(int i=0;i<256;i++){
        if (array[i] !=0)
            std::cout<<i<<'\t'<<char(i)<<'\t'<<array[i]<<std::endl;
    }
}
